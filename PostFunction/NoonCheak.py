import requests

def NoonInspection():
    headers = {
        'content-length' : '134',
        'cookie' : '',
        'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
        'content-type' : 'application/x-www-form-urlencoded',
        'token' : '',
        'refer' : 'https://servicewechat.com/wxce6d08f781975d91/143/page-frame.html',
        'accept-encoding' : 'gzip, deflate, br'
    }
    postdata = {
        'answers' : '["0"]',
        'seq' : '2',
        'temperature' : '36.5',
        'userid' : '',
        'latitude' : '',
        'longitude' : '',
        'country' : '',
        'city' : '',
        'district' : '',
        'province' : '',
        'township' : '',
        'street' : '',
        'myArea' : '',
        'areacode' : ''
    }
    url = 'https://student.wozaixiaoyuan.com/heat/save.json'
    s = requests.session()
    r = s.post(url, data=postdata, headers=headers)
    return r.text

if __name__ == "__main__":
    print(NoonInspection())