import requests

def HealthCheckIn():
    headers = {
        'content-length' : '296',
        'cookie' : '',
        # 'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat',
        'content-type' : 'application/x-www-form-urlencoded',
        'token' : '',
        'refer' : 'https://servicewechat.com/wxce6d08f781975d91/143/page-frame.html',
        'accept-encoding' : 'gzip, deflate, br'
        }
    Hpostdata = {
        'answers' : '["0"]',
        'seq' : '1',
        'temperature' : 36.3,
        'userId' : '',
        'latitude' : 34.108216,
        'longitude' : 108.605084,
        'country' : '中国',
        'city' : '西安市',
        'district' : '鄠邑区',
        'province' : '陕西省',
        'township' : '甘亭街道',
        'street' : '东街',
        'myArea' :''
        }
    # 我在校园
    url = 'https://student.wozaixiaoyuan.com/heat/save.json'
    s = requests.session()
    r = s.post(url, data=Hpostdata,headers=headers)
    return r.text
if __name__ == "__main__":
    print(HealthCheckIn())





