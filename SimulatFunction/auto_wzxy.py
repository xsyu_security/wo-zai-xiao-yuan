import os
import time
import cv2
import numpy as np
import smtplib
import email
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage

swipe_cmd = 'adb shell input swipe '
tap_cmd = 'adb shell input, , , , , , , ,  ,  ,  tap '

def open_wechat():
    #adb 调起WeChat
    os.system('adb shell am start -n com.tencent.mm/.ui.LauncherUI')
    time.sleep(5)

def open_vxpackage():
    '''打开进入【我在校园】小程序'''
    # 下滑拉出小程序菜单
    # 点击第一个图标位置（我在校园）

    os.system(swipe_cmd+'500 400 500 1200')
    # 滑动起始x坐标 起始y坐标 终点x 终点y

    time.sleep(5)
    os.system(tap_cmd+'185 620')
    # 点击x y

    time.sleep(5)

def morning_cheak():
    '''从我在校园主页面开始，进行晨检'''
    # 点击‘日检日报’ -- 选择‘晨检’ -- 选择’无症状‘ -- 滑动选择体温 --
    # 点击‘定位’ -- 点击‘提交’ -- 提交成功点击‘确定’ -- 
    # 点击左上角‘返回’，回到日检日报选择页面，截图发送，再返回我在校园主页面
    
    os.system(tap_cmd+'在此输入日检日报图标坐标，空格隔开')
    time.sleep(2)
    os.system(tap_cmd+'已经进入晨午检选择页面，在此输入晨检图标坐标')
    time.sleep(2)
    os.system(tap_cmd+'进入晨检页面，输入无症状勾选框坐标')
    time.sleep(2)
    os.system(tap_cmd+'输入选择体温坐标')
    time.sleep(2)
    os.system(swipe_cmd+'输入滑动选择体温的起止坐标，四个数字')
    time.sleep(2)
    os.system(tap_cmd+'输入点击定位坐标')
    time.sleep(2)
    os.system(tap_cmd+'输入提交图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'输入提交完的确定按钮坐标')
    time.sleep(2)
    os.system(tap_cmd+'左上角返回坐标')
    time.sleep(2)
    os.system('adb shell screencap -p /sdcard/result/morning_result.png')
    # 打卡结果截图保存在 /sdcard/result/ 目录， 提前创建好。

    time.sleep(2)
    os.system(tap_cmd+'再输入返回坐标')
    time.sleep(3)
    os.system(tap_cmd+'右上角圆圈图标，退出小程序')


def noon_cheak():
    # 从我在校园主页面开始
    # 点击日检日报 -- 选择午检 -- 点击无症状 -- 滑动输入体温 --
    # 提交 -- 确定 -- 返回两次
    os.system(tap_cmd+'在此输入日检日报图标坐标，空格隔开')
    time.sleep(2)
    os.system(tap_cmd+'已经进入晨午检选择页面，在此输入午检图标坐标')
    time.sleep(2)
    os.system(tap_cmd+'进入午检页面，输入无症状勾选框坐标')
    time.sleep(2)
    os.system(tap_cmd+'输入选择体温坐标')
    time.sleep(2)
    os.system(swipe_cmd+'输入滑动选择体温的起止坐标，四个数字')
    time.sleep(2)
    os.system(tap_cmd+'输入提交图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'输入提交完的确定按钮坐标')
    time.sleep(2)
    os.system(tap_cmd+'左上角返回坐标')
    time.sleep(2)
    os.system('adb shell screencap -p /sdcard/result/noon_result.png')
    time.sleep(2)
    os.system(tap_cmd+'再输入返回坐标')
    time.sleep(3)
    os.system(tap_cmd+'右上角圆圈图标，退出小程序')


def sign():
    # 从我在校园主页面开始
    # 点击签到 -- 选择第一个签到栏目的‘签到’选项 -- 点击定位图标 -- 
    # 点击提交 -- 提交成功点击确定 -- 点击返回两次
    os.system(tap_cmd+'签到图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'第一个签到项目的签到选项坐标')
    time.sleep(5)
    os.system(tap_cmd+'定位图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'提交图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'确定提交的图标坐标')
    time.sleep(5)
    os.system(tap_cmd+'签到成功后确认图标')
    time.sleep(3)
    os.system(tap_cmd+'返回图标')
    time.sleep(2)
    os.system('adb shell screencap -p /sdcard/result/sign_result.png')
    time.sleep(3)
    os.system(tap_cmd+'再次点击返回')
    time.sleep(3)
    os.system(tap_cmd+'右上角圆圈图标，退出小程序')

def do_u_log_in():

    print('登录状态检查。。。')
    '''预先在当前目录保存一张已经登录的个人信息页面截图的下半部分，每次
       程序运行之前，在个人信息页面截取图片，与保存的图片对比，
       若图片相同，则已经登陆。为了避免不同时段 状态栏的时间、
       信号强度、网络状态和其它通知图标的差异造成的干扰，只截取
       图片的下半部分进行对比。'''

       # 假设已经保存的已经录截图为“ logged.png ”

    os.system(tap_cmd+'右下角【我】图标坐标')
    os.system('adb shell screencap -p /sdcard/Download/screen.png')
    os.system('adb pull /sdcard/Download/screen.png')
    # 截图文件命名为 screen.png 每次截图会覆盖原文件

    image_logged = cv2.imread('./logged.png')
    image_screen = cv2.imread('./screen.png')

    screen_high = image_screen.shape[0]
    screen_weid = image_screen.shape[1]

    image_part_screen_arry = image_screen[int(screen_high/2):screen_high, 0:screen_weid]
    cv2.imwrite('./part_screen.png', image_part_screen_arry)
    image_part_screen = cv2.imread('./part_screen.png')

    difference = cv2.subtract(image_logged, image_part_screen)
    result = not np.any(difference)
 
    if result:
        print('已经登录')
        else:
            print('需要登录,正在执行登录操作')

            os.system(tap_cmd+'点击登录按钮的坐标')
            time.sleep(3)
            os.system(tap_cmd+'确认登录按钮坐标')
            time.sleep(5)
            print('登录完成')

def send_mail(mail_title, picture_path, picture_file_name):
    #!!!!!  传参时 picture_file_name 必须为双引号字符串

    host = 'mail.xsyu.edu.cn'
    user_name = '201712050715@stumail.sxyu.edu.cn'
    password = 'assiyvril9'
    sender_address = '201712050715@stumail.sxyu.edu.cn'
    receivers = ['leamant@qq.com',
                'wzxy@zjyu.me',
                'chenjiagen88@gmail.com']
    title = mail_title

    letter = email.mime.multipart.MIMEMultipart()
    letter['From'] = sender_address
    letter['To'] = receivers
    letter['Subject'] = title

    with open('./message.html','r') as message:
        content = message.read()
    part1 = email.mime.text.MIMEText(content,'html', 'utf-8')

    with open(picture_path, 'rb') as picture:
        image_be_sent = email.mime.image.MIMEImage(picture.read())
        image_be_sent['Content-type'] = 'application/octet-stream'
        image_be_sent['Content-Disposition'] = 'attachment;filename=' + picture_file_name

    letter.attach(part1)
    letter.attach(image_be_sent)
    try:        
        send_operating = smtplib.SMTP_SSL()
        send_operating.connect(host=host, port=25)
        send_operating.login(user=user_name, password=password)
        send_operating.sendmail(
            from_addr=sender_address,
            to_addrs=receivers,
            msg=letter.as_string())
        print('send SUCCESS!')
        send_operating.quit()
        except smtplib.SMTPResponseException as error:
            print('Faild Send', error)


def morrning_operating():
    '''执行操作流程'''

    open_wechat()
    # 打开微信

    open_vxpackage()
    # 打开小程序

    do_u_log_in()
    # 检测登录状态

